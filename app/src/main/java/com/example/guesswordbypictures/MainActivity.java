package com.example.guesswordbypictures;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private final String TAG = this.getClass().getSimpleName();

    public TextView levelTitle;

    public TableLayout tableWithPictures;

    public ImageView pictureOne;
    public ImageView pictureTwo;
    public ImageView pictureThree;
    public ImageView pictureFour;

    public EditText userWordText;

    public Button checkWordButton;

    private Question[] questions;
    private Question currentQuestion;
    private int currentLevel;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        levelTitle = findViewById(R.id.levelCounter);

        tableWithPictures = findViewById(R.id.tableWithPictures);
        tableWithPictures.setOnTouchListener(new OnSwipeTouchListener(this) {
            @Override
            public void onSwipeLeft() {
                nextRound();
            }

            @Override
            public void onSwipeRight() {
                previousRound();
            }
        });

        pictureOne = findViewById(R.id.questionPictureOne);
        pictureTwo = findViewById(R.id.questionPictureTwo);
        pictureThree = findViewById(R.id.questionPictureThree);
        pictureFour = findViewById(R.id.questionPictureFour);

        userWordText = findViewById(R.id.userWordText);

        checkWordButton = findViewById(R.id.checkWordButton);
        checkWordButton.setOnClickListener(this);

        questions = Question.generateQuestions();

        currentQuestion = questions[currentLevel];
        setQuestionPictures();
        setLevelTitle();
    }

    private void setLevelTitle() {
        Log.i(TAG, "call setLevelTitle");
        levelTitle.setText(String.format("Уровень %s", currentLevel + 1));
    }

    private void setQuestionPictures() {
        Log.i(TAG, "call setQuestionPictures");
        String[] picturesName = currentQuestion.getPictures();

        pictureOne.setImageResource(getResources().getIdentifier(picturesName[0], "drawable", this.getPackageName()));
        pictureOne.startAnimation(alphaScaleRotateAnimation(500));

        pictureTwo.setImageResource(getResources().getIdentifier(picturesName[1], "drawable", this.getPackageName()));
        pictureTwo.startAnimation(alphaScaleRotateAnimation(1000));

        pictureThree.setImageResource(getResources().getIdentifier(picturesName[2], "drawable", this.getPackageName()));
        pictureThree.startAnimation(alphaScaleRotateAnimation(1500));

        pictureFour.setImageResource(getResources().getIdentifier(picturesName[3], "drawable", this.getPackageName()));
        pictureFour.startAnimation(alphaScaleRotateAnimation(2000));
    }

    private AnimationSet alphaScaleRotateAnimation(int duration) {
        Animation alphaAnimation = new AlphaAnimation(0.1f, 1.0f);
        ScaleAnimation scaleAnimation = new ScaleAnimation(0.1f, 1.0f, 0.1f, 1.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        RotateAnimation rotateAnimation = new RotateAnimation(0.0f, 720.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        AnimationSet animationSet = new AnimationSet(false);

        alphaAnimation.setDuration(duration);
        scaleAnimation.setDuration(duration);
        rotateAnimation.setDuration(duration);
        animationSet.addAnimation(alphaAnimation);
        animationSet.addAnimation(scaleAnimation);
        animationSet.addAnimation(rotateAnimation);
        return animationSet;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Log.i(TAG, "Create option menu");
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBackButton:
                Log.i(TAG, "select menuBackButton");
                previousRound();
                return true;
            case R.id.menuForwardButton:
                Log.i(TAG, "select menuForwardButton");
                nextRound();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.checkWordButton:
                Log.i(TAG, "Click on checkWordButton");
                checkAnswer();
                break;
        }
    }

    private void checkAnswer() {
        String userInput = userWordText.getText().toString().toLowerCase();
        Log.i(TAG, String.format("User input is '%s'", userInput));
        Log.i(TAG, String.format("Current question answer is '%s'", currentQuestion.getRightAnswer().toLowerCase()));

        if (userInput.equals(currentQuestion.getRightAnswer().toLowerCase())) {
            Toast toast = Toast.makeText(this, "Правильный ответ", Toast.LENGTH_SHORT);
            toast.show();
            nextRound();
        } else {
            Toast toast = Toast.makeText(this, "Неправильный ответ", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void nextRound() {
        currentLevel++;
        if (currentLevel < questions.length) {
            currentQuestion = questions[currentLevel];
            setQuestionPictures();
            setLevelTitle();
        } else {
            currentLevel = questions.length - 1;
            Toast toast = Toast.makeText(this, "Вопросов больше нет", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private void previousRound() {
        currentLevel--;
        if (currentLevel < 0) {
            currentLevel = 0;
            Toast toast = Toast.makeText(this, "Предыдущего уровня нет", Toast.LENGTH_SHORT);
            toast.show();

        } else {
            currentQuestion = questions[currentLevel];
            setQuestionPictures();
            setLevelTitle();
        }
    }
}
