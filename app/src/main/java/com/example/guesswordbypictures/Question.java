package com.example.guesswordbypictures;

public class Question {
    private static String pictureStringFmt = "question_%s_pic_%s";
    private String rightAnswer;
    private String[] pictures;

    public Question(String[] pictures, String rightAnswer) {
        this.pictures = pictures;
        this.rightAnswer = rightAnswer;
    }

    public static Question[] generateQuestions() {
        final int questionQuantity = 5;
        final int picturesQuantity = 4;

        String[] answers = new String[]{"Earth", "Fire", "Water", "Air", "Lightning"};
        Question[] questions = new Question[questionQuantity];

        for (int questionNumber = 0; questionNumber < questionQuantity; questionNumber++) {
            String[] pictures = new String[picturesQuantity];

            for (int pictureNumber = 0; pictureNumber < picturesQuantity; pictureNumber++) {
                pictures[pictureNumber] = String.format(pictureStringFmt, questionNumber + 1, pictureNumber + 1);
            }
            questions[questionNumber] = new Question(pictures, answers[questionNumber]);
        }
        return questions;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public String[] getPictures() {
        return pictures;
    }
}
